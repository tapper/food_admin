import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CustomMealRoutes} from './custom-meal.routing';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {IndexComponent} from './index/index.component';

import {FileUploadModule} from 'ng2-file-upload/ng2-file-upload';
import {TreeModule} from 'angular-tree-component';
import {CustomFormsModule} from 'ng2-validation';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule, NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import {TextMaskModule} from 'angular2-text-mask';
import {LoadingModule} from 'ngx-loading';
import {AngularMultiSelectModule} from 'angular2-multiselect-dropdown';
import {MomentModule} from 'angular2-moment';

@NgModule({
    imports: [CommonModule,
        RouterModule.forChild(CustomMealRoutes),
        NgxDatatableModule,
        FormsModule,
        ReactiveFormsModule,
        NgbProgressbarModule,
        CustomFormsModule,
        TreeModule,
        FileUploadModule,
        LoadingModule,
        TextMaskModule,
        AngularMultiSelectModule,
        MomentModule,
        NgbModule
    ],
    declarations: [IndexComponent]
})

export class CompaniesModule {}