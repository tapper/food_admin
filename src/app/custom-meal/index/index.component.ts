import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from "@angular/router";
import * as moment from 'moment';
import { MomentModule } from 'angular2-moment';


@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    employeesArray: Array<any>;
    resturantsArray: Array<any>;
    orderDetailsArray: Array<any>;
    start = moment();
    todaydate = moment(this.start).format("YYYY-MM-DD");
    formatteddate = moment(this.start).format("DD/MM/YY");
    end = moment();
    company_id: number;
    gap: string = 'days';
    types = {main_dish: 0, salat: 0, sandwich: 0, fastfood: 0};
    menuTypesArray1: Array<any> = ["מנה מוגשת","כריכים","סלטים","מנה ארוזה"];
    menuTypesArray2: Array<any> = ["תוספות","רטבים","לחם","שתיה"];
    resturantFirstDishMenuArray : Array<any>;
    resturantTosafotArray : Array<any>;
    resturantRetavimArray : Array<any>;
    resturantBreadArray : Array<any>;
    resturantDrinksArray : Array<any>;
    sent: boolean = false;
    admin: boolean = false;
    selectedresurant : number;
    selectedfirstdishtype : number;
    selectedfirstdish : number;
    tosefetQuanArray = [];
    fastfood:any;
    confirmModal: any;
    SavedModal: any;
    orderDetailsModal: any;
    saveToRow : any;


    savedDataArray =
     {
         employee: '',
         restrurant: '',
         manatype: '',
         firstdish: '',
         tosefet: '',
         retavim: '',
         bread: '',
         drink: '',
         quan: '',
         remarks: '',
     };


    constructor(public api: ApiService, public modalService: NgbModal,public activatedRoute: ActivatedRoute) { }

    ngOnInit() {

        this.admin = localStorage.getItem('type') == '0';
        this.sent = false;
        this.activatedRoute.params.subscribe(async (data) => {
            this.company_id = data.id;
            this.getEmployees(this.company_id);
            this.getResturants(this.company_id);
        });


        for (let i = 1; i <= 20; i++) {
            this.tosefetQuanArray.push(i);
        }
    }

    getEmployees (id) {
        this.employeesArray = [];
        this.api.sendPost('webGetDailyEmployeesMenu', {company_id: id,day: moment(this.start).format("YYYY-MM-DD")}).subscribe(data => {
            this.employeesArray = data;
            console.log("webGetDailyEmployeesMenu",this.employeesArray)
            for (let row of this.employeesArray) {
                if (row.selectresturant && row.selectedfirstdishtype)
                {
                    this.getFirstDish(row,row.selectresturant,row.selectedfirstdishtype);
                    row.manatype_title = this.menuTypesArray1[row.selectedfirstdishtype];
                }

            }
        });
    }

    getResturants (id) {
        this.api.sendPost('getUserResturants', {companyid: id}).subscribe(data => {
            this.resturantsArray = data;
            console.log("resturantsArray",this.resturantsArray)
        });
    }

    getOrderDetails(id,content)
    {
        this.api.sendPost('webOrderDetailsById', {order_id: id}).subscribe(data => {
            this.orderDetailsArray = data.orders;

            if (this.orderDetailsArray.length){
                for (let row of this.orderDetailsArray){
                    switch (row.food_type){
                        case '0':
                            this.types.main_dish += 1;
                            break;
                        case '1':
                            this.types.salat += 1;
                            break;
                        case '2':
                            this.types.sandwich += 1;
                            break;
                        case '3':
                            this.types.fastfood += 1;
                            break;
                        default:
                            break;
                    }
                }
            } else {
                this.types = {main_dish: 0, salat: 0, sandwich: 0, fastfood: 0};
            }

            console.log("webOrderDetailsById",this.orderDetailsArray);
            this.orderDetailsModal =  this.modalService.open(content);
        });
    }

    closeOrderDetailsModal()
    {
        this.orderDetailsModal.close();
    }

    onChangeResturant(row,event) {
        let targetvalue = event.target;
        this.selectedresurant = targetvalue.value;
        row.resturant_title = targetvalue.options[targetvalue.selectedIndex].getAttribute('valueTitle');;

        //reset all
        row.manatype_title = "";
        row.firstdish_title = "";
        row.tosefet_title = "";
        row.retavim_title = "";
        row.bread_title = "";
        row.drink_title = "";

        row.selectedfirstdishtype = "";
        row.selectmainfood = "";
        row.selectedtosefet = "";
        row.selectedretavim = "";
        row.selectedbread = "";
        row.selecteddrink = "";
        row.selectquan = "1";
    }

    getFirstDish(row,resturant_id,food_type)
    {
        this.api.sendPost('webgetResturantMenu', {resturant_id:  resturant_id,food_type: food_type}).subscribe(data => {
            row.resturantFirstDishMenuArray = data.firstdish;
            row.resturantTosafotArray = data.tosefet;
            row.resturantRetavimArray = data.retavim;
            row.resturantBreadArray = data.bread;
            row.resturantDrinksArray = data.drinks;
            console.log("getFirstDish",data);
        });
    }

    onChangeFirstDishType(row,event) {

        let targetvalue = event.target;
        this.selectedfirstdishtype = targetvalue.value;
        row.manatype_title = targetvalue.options[targetvalue.selectedIndex].getAttribute('valueTitle');

        if (row.selectresturant && row.selectedfirstdishtype)
        {
            this.api.sendPost('webgetResturantMenu', {resturant_id:  row.selectresturant,food_type: row.selectedfirstdishtype}).subscribe(data => {

                row.resturantFirstDishMenuArray = data.firstdish;
                row.resturantTosafotArray = data.tosefet;
                row.resturantRetavimArray = data.retavim;
                row.resturantBreadArray = data.bread;
                row.resturantDrinksArray = data.drinks;

                //reset all
                row.firstdish_title = "";
                row.tosefet_title = "";
                row.retavim_title = "";
                row.bread_title = "";
                row.drink_title = "";

                row.selectmainfood = "";
                row.selectedtosefet = "";
                row.selectedretavim = "";
                row.selectedbread = "";
                row.selecteddrink = "";
                row.selectquan = "1";
            });
        }
    }

    onChangeSelectFirstDish(row,event) {
        let targetvalue = event.target;
        this.selectedfirstdish = targetvalue.value;
        row.firstdish_title = targetvalue.options[targetvalue.selectedIndex].getAttribute('valueTitle');
    }

    onChangeSelectTosefet(row,event) {
        let targetvalue = event.target;
        row.tosefet_title = targetvalue.options[targetvalue.selectedIndex].getAttribute('valueTitle');
    }

    onChangeSelectRetavim(row,event) {
        let targetvalue = event.target;
        row.retavim_title = targetvalue.options[targetvalue.selectedIndex].getAttribute('valueTitle');
    }

    onChangeSelectBread(row,event) {
        let targetvalue = event.target;
        row.bread_title = targetvalue.options[targetvalue.selectedIndex].getAttribute('valueTitle');
    }

    onChangeSelectDrink(row,event) {
        let targetvalue = event.target;
        row.drink_title = targetvalue.options[targetvalue.selectedIndex].getAttribute('valueTitle');
    }


    openConfirmSaveModal(content, row) {

        if (!row.selectquan)
            row.selectquan = 1;


        if (!row.selectresturant)
        {
            alert ("יש לבחור מסעדה");
        }
        else if (!row.selectedfirstdishtype)
        {
            alert ("יש לבחור סוג מנה");
        }

        else if (!row.selectmainfood)
        {
            alert ("יש לבחור מנה ראשונה");
        }
        else
        {
            this.saveToRow = row;
            this.savedDataArray.employee = row.name;
            this.savedDataArray.restrurant = row.resturant_title;
            this.savedDataArray.manatype = row.manatype_title;
            this.savedDataArray.firstdish = row.firstdish_title;
            this.savedDataArray.tosefet = row.tosefet_title;
            this.savedDataArray.retavim = row.retavim_title;
            this.savedDataArray.bread = row.bread_title;
            this.savedDataArray.drink = row.drink_title;
            this.savedDataArray.quan = row.selectquan;
            this.savedDataArray.remarks = row.selectremarks;
            this.confirmModal = this.modalService.open(content);
        }

        console.log("BeforesaveTable",row);
    }

    saveTable(content) {
        this.confirmModal.close();
        console.log("saveTable",this.saveToRow);
        this.sent = false;

        if (!this.saveToRow.mod1)
            this.saveToRow.mod1 = 0;

        if (!this.saveToRow.mod2_first)
            this.saveToRow.mod2_first = 0;

        if (!this.saveToRow.mod2_second)
            this.saveToRow.mod2_second = 0;


        if (!this.saveToRow.drink)
            this.saveToRow.drink = 0;

        if (!this.saveToRow.selectremarks)
            this.saveToRow.selectremarks = '';

        if (this.saveToRow.selectedfirstdishtype == 3)
            this.fastfood = this.saveToRow.selectmainfood;
        else
            this.fastfood = 0;

        this.api.sendPost('webSaveCustomOrder', {
                'user_id':  this.saveToRow.index,
                'company_id':  this.company_id,
                'restaurant_id':  this.saveToRow.selectresturant,
                'food_type':  this.saveToRow.selectedfirstdishtype,
                'main':  this.saveToRow.selectmainfood,
                'mod1':  this.saveToRow.selectedbread,
                'mod2_first':  this.saveToRow.selectedtosefet,
                'mod2_second':  this.saveToRow.selectedretavim,
                'drink':  this.saveToRow.selecteddrink,
                'fastfood':  this.fastfood,
                'comments':  this.saveToRow.selectremarks,
                'quantity':  this.saveToRow.selectquan,
                'order_date':  moment(this.start).format("YYYY-MM-DD"),
            }
            ).subscribe(data =>
        {
            console.log("savedData",data);
            this.SavedModal =  this.modalService.open(content);
            if (data.status == 1)
            {
                this.saveToRow.order_exists = 1;
                this.saveToRow.order_id = data.order_id;
                this.sent = true;
            }
            else
            {
                this.sent = false;
            }
        });
    }

    closeSaveModal() {
        this.SavedModal.close();
    }



    assignDates(settings: string){
        this.sent = false;
        if (settings === 'forward'){
            switch (this.gap){
                case 'days':
                    this.start = moment(Object.assign({}, this.start)).add(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).add(1, 'days');
                    this.getEmployees(this.company_id);
                    break;
                case 'weeks':
                    this.start = moment(Object.assign({}, this.start)).add(1, 'weeks');
                    this.end = moment(Object.assign({}, this.end)).add(1, 'weeks');
                    this.getEmployees(this.company_id);
                    break;
                case 'months':
                    this.start = moment(Object.assign({}, this.start)).add(1, 'months');
                    this.end = moment(Object.assign({}, this.start)).endOf('month');
                    this.getEmployees(this.company_id);
                    break;
                default:
                    this.start = moment(Object.assign({}, this.start)).add(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).add(1, 'days');
                    this.getEmployees(this.company_id);
                    break;
            }
        }
        if (settings === 'backward'){
            switch (this.gap){
                case 'days':
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).subtract(1, 'days');
                    this.getEmployees(this.company_id);
                    break;
                case 'weeks':
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'weeks');
                    this.end = moment(Object.assign({}, this.end)).subtract(1, 'weeks');
                    this.getEmployees(this.company_id);
                    break;
                case 'months':
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'months');
                    this.end = moment(Object.assign({}, this.start)).endOf('month');
                    this.getEmployees(this.company_id);
                    break;
                default:
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).subtract(1, 'days');
                    this.getEmployees(this.company_id);
                    break;
            }
        }
    }

}
