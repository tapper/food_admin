import {ChangeDetectionStrategy, Component, OnInit, NgZone, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators, NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';
import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
//import moment = require("moment");
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
})



export class EditComponent implements OnInit {

    @ViewChild('f') f: NgForm;
    public loading = false;
    resturantsArray: Array<any> = [];
    menuTypesArray1: Array<any> = ["מנה מוגשת","כריכים","סלטים","מנה ארוזה"];
    menuTypesArray2: Array<any> = ["תוספות","רטבים","לחם","שתיה"];
    resturantFirstDishMenuArray  : Array<any> = [];
    firstdish : Array<any> = [];
    searchArray : Array<any>;
    resturantTosafotArray : Array<any> = [];
    resturantRetavimArray : Array<any>;
    resturantBreadArray : Array<any>;
    resturantDrinksArray : Array<any>;
    tosefetQuanArray = [];
    fastfood:any;
    dataArray: any = [];

    selectedTosafot: Array<any> = [];
    //public _selectedTosafot = new Subject<any>();
    //selectedTosafot$: Observable<any> = this._selectedTosafot.asObservable();

    errors: Array<any> = [];
    dropdownSettings = {
        singleSelection: false,
        text:"בחירת תוספות",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        classes:"custom-select-item",
        limitSelection : 4,
    };
    public company_id:any;
    public order_id:any;
    model: NgbDateStruct;
    date: {year: number, month: number};
    now = new Date();


    itemList = [];
    selectedItems = [];
    settings = {};
    admin: boolean = false;




    public fields:any = {
        "fullname" : "",
        "remarks": "",
        "resturant_id" : "",
        "dishtype" : "",
        "firstdish" : "",
        "tosafot" : [],
        "tosefet_id" : "",
        "retavim_id" : "",
        "bread_id" : "",
        "drink_id" : "",
        "tosefet_quan" : "1",
        "deliverydate" : ""
    }




    constructor(public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public api: ApiService , private route: ActivatedRoute, public zone: NgZone) {

        this.route.queryParams.subscribe(params => {
            this.order_id =   params['order_id'];
            this.getFirstDishOrderById(this.order_id);
            this.company_id =   params['company_id'];
            this.getResturants(this.company_id);
        });


        /*
        this._selectedTosafot.subscribe(val => {
            this.zone.run(() => {
                alert (444);
            });
        });
        */



        this.admin = localStorage.getItem('type') == '0';

    }

    async getFirstDishOrderById(order_id) {
             await this.api.sendPost('getFirstDishOrderById', {order_id: order_id}).subscribe(data => {
                this.dataArray = data;
                this.fields.resturant_id =  data[0].restaurant_id;
                this.onChangeResturant();
                this.fields.fullname =  data[0].fullname;
                this.fields.remarks =  data[0].comments;
                this.selectedTosafot =  data[0].tosafot;
                //this.selectedTosafot = JSON.stringify(data[0].tosafot);
                this.firstdish = data[0].firstdish;
                let splitdate = data[0].order_date.split("-");
                this.fields.deliverydate = {year: parseInt(splitdate[0]), month: parseInt(splitdate[1]), day: parseInt(splitdate[2])};
                console.log("selectedTosafot11",data[0].tosafot)
            });
    }

    getResturants (id) {
        this.api.sendPost('getUserResturants', {companyid: id}).subscribe(data => {
            this.resturantsArray = data;
            console.log("resturantsArray",this.resturantsArray)
        });
    }

    testing() {
        console.log("selectedTosafot14",this.selectedTosafot)
    }

    async onChangeResturant() {
        if  (this.fields.resturant_id) {
            await this.api.sendPost('webgetCompanyResturantMenuByOrderId', {resturant_id:  this.fields.resturant_id,
                order_id:  this.order_id}).subscribe(data => {
                this.resturantTosafotArray = data.tosefet;
                this.resturantFirstDishMenuArray = data.firstdish;
                //this._resturantFirstDishMenuArray.next(newResturantFirstDishMenuArray);
                console.log("resturantFirstDishMenuArray111",this.resturantFirstDishMenuArray)
            });
        }
    }




    async ngOnInit() {


        for (let i = 1; i <= 20; i++) {
            this.tosefetQuanArray.push(i);
        }
        //this.fields.deliverydate = {year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate()};
    }



     onSubmit(form: NgForm) {
        //console.log("form",form.value);

        //alert (form.value.tosafotArray)

        //console.log("selectedTosafot",this.selectedTosafot)
        //console.log("selectedTosafot",JSON.stringify(this.selectedTosafot))
        //alert (this.selectedTosafot.length)
        //alert (this.selectedTosafot);
        console.log("selectedTosafot22",this.selectedTosafot)



         this.api.sendPost('webUpdateCompanyFirstDishOrder', {
                //'company_id':  this.company_id,
                 'order_id':  this.order_id,
                'restaurant_id':  this.fields.resturant_id,
                'first_dish_array':  JSON.stringify(this.resturantFirstDishMenuArray) ,
                'tosafot_array':  JSON.stringify(this.selectedTosafot) ,
                //'tosafot_array': form.value.tosafot.map(value => {return value.index}),
                'comments':  this.fields.remarks,
                'fullname':  this.fields.fullname,
                'order_date_year':  this.fields.deliverydate.year,//moment(this.start).format("YYYY-MM-DD"),
                'order_date_month':  this.fields.deliverydate.month,//moment(this.start).format("YYYY-MM-DD"),
                'order_date_day':  this.fields.deliverydate.day,//moment(this.start).format("YYYY-MM-DD"),
                //'order_date':  this.fields.deliverydate//moment(this.start).format("YYYY-MM-DD"),
            }
        ).subscribe(data =>
        {
            console.log("savedData",data);

            if (this.admin)
                    this.router.navigateByUrl('/company_firstdish_order');
                else
                   this.router.navigateByUrl('/company_firstdish_order/'+this.company_id);
        });
    }

    updateFilter(event) {
        /*
        const val = event.target.value;
        // filter our data
        const temp = this.searchArray.filter(function(d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.resturantFirstDishMenuArray = temp;
        */
    }



}