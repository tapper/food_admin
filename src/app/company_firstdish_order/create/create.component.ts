import {ChangeDetectionStrategy, Component, OnInit, NgZone, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators, NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';
import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
///import moment = require("moment");
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {

    @ViewChild('f') f: NgForm;
    public loading = false;
    resturantsArray: Array<any>;
    menuTypesArray1: Array<any> = ["מנה מוגשת","כריכים","סלטים","מנה ארוזה"];
    menuTypesArray2: Array<any> = ["תוספות","רטבים","לחם","שתיה"];
    resturantFirstDishMenuArray : Array<any>
    searchArray : Array<any>;
    resturantTosafotArray : Array<any> = [];
    resturantRetavimArray : Array<any>;
    resturantBreadArray : Array<any>;
    resturantDrinksArray : Array<any>;
    tosefetQuanArray = [];
    fastfood:any;
    selectedTosafot: any = [];

    //public _selectedTosafot = new Subject<any>();
    //selectedTosafot$: Observable<any> = this._selectedTosafot.asObservable();


    errors: Array<any> = [];
    dropdownSettings = {
        singleSelection: false,
        text:"בחירת תוספות",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        classes:"custom-select-item",
        limitSelection : 4,
    };
    public company_id:any;
    model: NgbDateStruct;
    date: {year: number, month: number};
    now = new Date();


    itemList = [];
    selectedItems = [];
    settings = {};



    public fields:any = {
        "fullname" : "",
        "remarks": "",
        "resturant_id" : "",
        "dishtype" : "",
        "firstdish" : "",
        "tosafot" : "",
        "tosefet_id" : "",
        "retavim_id" : "",
        "bread_id" : "",
        "drink_id" : "",
        "tosefet_quan" : "1",
        "deliverydate" : ""
    }




    constructor(public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public api: ApiService , private route: ActivatedRoute, public zone: NgZone) {

        this.route.queryParams.subscribe(params => {
            this.company_id =   params['company_id'];
            this.getResturants(this.company_id);
        });

        /*
        this._selectedTosafot.subscribe(val => {
            this.zone.run(() => {
                this.selectedTosafot = val;
            });
        });
        */
    }

    getResturants (id) {
        this.api.sendPost('getUserResturants', {companyid: id}).subscribe(data => {
            this.resturantsArray = data;
            console.log("resturantsArray",this.resturantsArray)
        });
    }

    async onChangeResturant(event) {
        if  (this.fields.resturant_id) {
            await this.api.sendPost('webgetCompanyResturantMenu', {resturant_id:  this.fields.resturant_id}).subscribe(data => {
                this.resturantFirstDishMenuArray = data.firstdish;
                //for (let i = 0; i <= this.resturantFirstDishMenuArray.length; i++) {
                    //this.resturantFirstDishMenuArray[i].quan = "0";
                    //this.resturantFirstDishMenuArray[i].choosen = 0;
                //}
                //this.searchArray = this.resturantFirstDishMenuArray;
                this.resturantTosafotArray = data.tosefet;
            });
        }
    }

    onChangeFirstDishType(event) {
        //this.webgetResturantMenu();
    }

    async webgetResturantMenu() {

    }

    selectDish(row) {
        if (row.choosen == 0)
            row.choosen = 1;
        else
            row.choosen = 0;
    }


    onItemSelect(item:any){
        //console.log(item);
    }
    OnItemDeSelect(item:any){

    }
    onSelectAll(items: any){
        //console.log(items);
    }
    onDeSelectAll(items: any){
        //console.log(items);
    }

    async ngOnInit() {


        for (let i = 1; i <= 20; i++) {
            this.tosefetQuanArray.push(i);
        }
        this.fields.deliverydate = {year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate()};

    }



    onSubmit(form: NgForm) {
        console.log("form",form.value);



        //console.log("selectedTosafot",this.selectedTosafot)
        console.log("selectedTosafot",JSON.stringify(this.selectedTosafot))



        this.api.sendPost('webSaveCompanyFirstDishOrder', {
                'company_id':  this.company_id,
                'restaurant_id':  this.fields.resturant_id,
                'first_dish_array':  JSON.stringify(this.resturantFirstDishMenuArray) ,
                'tosafot_array':  JSON.stringify(this.selectedTosafot) ,
                'comments':  this.fields.remarks,
                'fullname':  this.fields.fullname,
                'order_date_year':  this.fields.deliverydate.year,//moment(this.start).format("YYYY-MM-DD"),
                'order_date_month':  this.fields.deliverydate.month,//moment(this.start).format("YYYY-MM-DD"),
                'order_date_day':  this.fields.deliverydate.day,//moment(this.start).format("YYYY-MM-DD"),
            //'order_date':  this.fields.deliverydate//moment(this.start).format("YYYY-MM-DD"),
            }
        ).subscribe(data =>
        {
            console.log("savedData",data);
            this.router.navigateByUrl('/company_firstdish_order/'+this.company_id);
        });


    }

    updateFilter(event) {
        const val = event.target.value;
        // filter our data
        const temp = this.searchArray.filter(function(d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.resturantFirstDishMenuArray = temp;
    }



}