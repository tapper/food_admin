import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {

    public loading = false;
    kitchens: Array<any> = [];
    company: any;
    form: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        phone: new FormControl('', Validators.required),
        address: new FormControl('', Validators.required),
        email: new FormControl('', Validators.required),
        username: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
        price: new FormControl('', Validators.required),
        description: new FormControl('', Validators.required),
        kitchens: new FormControl(null, Validators.required),
    });
    logo = {path: null, file: null};
    errors: Array<any> = [];
    dropdownSettings = {
        singleSelection: false,
        text:"Select kitchens",
        selectAllText:'Select All',
        unSelectAllText:'UnSelect All',
        classes:"custom-select-item"
    };

    constructor(public fb: FormBuilder,
                public activatedRoute: ActivatedRoute,
                private router: Router,
                private sanitizer: DomSanitizer,
                public api: ApiService) {}

    async ngOnInit() {

        const quill = new Quill('#editor-container', {
            modules: {toolbar: {container: '#toolbar-toolbar'}},
            theme: 'snow'
        });

        quill.on('text-change', () => {
            this.form.controls.description.setValue(quill.root.innerHTML);
        });

        this.activatedRoute.params.subscribe(async (data) => {
            this.api.sendPost('webGetCompany', {company_id: data.id}).subscribe(response => {
                this.company = response;

                let kitchens = [];
                for (let item of this.company.kitchens){
                    item.kitchen.itemName = item.kitchen.name;
                    kitchens.push(item.kitchen);
                }

                this.form.setValue({
                    name: this.company.name,
                    phone: this.company.phone,
                    address: this.company.address,
                    email: this.company.email,
                    username: this.company.username,
                    password: this.company.pass,
                    price: this.company.resturant_price,
                    description: this.company.desc,
                    kitchens: kitchens
                });

                this.logo.path = environment.apiEndpoint + this.company.image;
                quill.clipboard.dangerouslyPasteHTML(this.company.desc);
            })
        });

        this.getKitchens();
    }

    getKitchens () {
        this.api.sendGet('webGetKitchens').subscribe(data => {
            for (let kitchen of data){
                kitchen.itemName = kitchen.name;
            }
            this.kitchens = data;
        });
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        if (this.logo.file === null && this.logo.path == ''){
            this.errors.push({message: "Logo is empty!"});
            return;
        }

        this.loading = true;

        let payload: any = {};
        payload.id = this.company.index;
        payload.name = this.form.value.name;
        payload.phone = this.form.value.phone;
        payload.address = this.form.value.address;
        payload.email = this.form.value.email;
        payload.username = this.form.value.username;
        payload.password = this.form.value.password;
        payload.price = this.form.value.price;
        payload.description = this.form.value.description;
        payload.image = this.logo.file;
        payload.kitchens = this.form.value.kitchens.map(value => {return value.index});

        console.log(payload);
        this.api.sendPost('webUpdateCompany', payload).subscribe(data => {

            this.loading = false;
            this.router.navigate(['/companies']);

        }, error => {

            this.loading = false;
            this.errors.push({message: "Something is wrong, the company wasn't updated"});

        });

    }

}