import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    rows: Array<any>;
    companies: Array<any>;
    deleteModal: any;
    pushModal: any;
    companyToDelete: any;
    companyToPush: any;
    pushText: string = '';
    sent: boolean = false;
    inProcess: boolean = false;

    constructor(public api: ApiService, public modalService: NgbModal) { }

    ngOnInit() {
        this.getCompanies();
    }

    getCompanies () {
        this.api.sendGet('webGetCompanies').subscribe(data => {
            this.rows = data;
            this.companies = this.rows;
        });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.companies.filter(function(d) {
            return d.name && d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteCompany(){
        this.api.sendPost('webDeleteCompany', {company_id: this.companyToDelete.index}).subscribe(data => {
            this.getCompanies();
        });
        this.deleteModal.close();
    }

    openDeleteModal(content, company) {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = company;
    }

    closePushModal () {
        this.pushModal.close();
        this.sent = false;
    }

    openPushModal (content, company) {
        this.pushModal = this.modalService.open(content);
        this.companyToPush = company;
    }

    async sendPush(){
        this.inProcess = true;
        let index = this.companyToPush == null ? 0 : this.companyToPush.index;
        this.api.sendPost('webSendPush', {company_id: index, text: this.pushText}).subscribe(data => {
            this.pushText = '';
            this.sent = true;
            this.inProcess = false;
        }, error => {this.inProcess = false;});
    }

}
