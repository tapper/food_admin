import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {CreateComponent} from './create/create.component';
import {EditComponent} from './edit/edit.component';

export const EmployeesRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'Employees'},
        },
        {
            path: 'create',
            component: CreateComponent,
            data: {heading: 'New employee'},
        },
        {
            path: 'edit/:id',
            component: EditComponent,
            data: {heading: 'Edit employee'},
        }
    ]
}];
