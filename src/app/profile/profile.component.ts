import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../_services/api/api.service';
import {environment} from '../../environments/environment';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class IndexComponent implements OnInit {

    saved: boolean = false;
    form1: FormGroup = this.fb.group({
        name: new FormControl('', Validators.required),
        username: new FormControl('', Validators.required),
    });
    form2: FormGroup = this.fb.group({
        new_password: new FormControl('', Validators.required),
        old_password: new FormControl('', Validators.required),
    });
    logo = {path: null, file: null};

    constructor(public fb: FormBuilder, public api: ApiService, public sanitizer: DomSanitizer) {}

    ngOnInit() {
        this.api.sendPost('webGetProfile', {user_id: localStorage.getItem('id')}).subscribe(data => {
            this.form1.setValue({name: data.name, username: data.username});
            if (data.image){
                this.logo.path = environment.apiEndpoint + data.image;
            }
        });
    }

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }

    async onSubmit () {
        this.saved = false;
        let payload: any = {
            user_id: localStorage.getItem('id'),
            name: this.form1.value.name,
            username: this.form1.value.username
        };
        if (this.logo.file !== null){
            payload.image = this.logo.file;
        }
        this.api.sendPost('webUpdateProfile', payload).subscribe(data => {
            localStorage.setItem('image', data.image);
            this.saved = true;
        });
    }

    async onSubmitPassword(){
        this.saved = false;
        this.api.sendPost('webUpdatePassword', {
            user_id: localStorage.getItem('id'),
            old_password: this.form2.value.old_password,
            new_password: this.form2.value.new_password
        }).subscribe(data => {
            if (data.status == 1){
                this.saved = true;
                this.form2.reset();
            } else {
                alert('The password was not changed because the old password is incorrect');
            }
        });
    }

}
