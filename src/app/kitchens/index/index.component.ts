import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    rows: Array<any>;
    kitchens: Array<any>;
    deleteModal: any;
    returnModal: any;
    itemToDelete: any;
    itemToReturn: any;

    constructor(public api: ApiService, public modalService: NgbModal) { }

    ngOnInit() {
        this.getKitchens();
    }

    getKitchens () {
        this.api.sendGet('webGetKitchens').subscribe(data => {
            this.rows = data;
            this.kitchens = this.rows;
        });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.kitchens.filter(function(d) {
            return d.name && d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteKitchen(){
        this.api.sendPost('webDeleteKitchen', {kitchen_id: this.itemToDelete.index}).subscribe(data => {
            this.getKitchens();
        });
        this.deleteModal.close();
    }

    openDeleteModal(content, item) {
        this.deleteModal = this.modalService.open(content);
        this.itemToDelete = item;
    }

    openReturnModal(content, item) {
        this.returnModal = this.modalService.open(content);
        this.itemToReturn = item;
    }

    async returnKitchen(){
        this.api.sendPost('webReturnKitchen', {kitchen_id: this.itemToReturn.index}).subscribe(data => {
            this.getKitchens();
        });
        this.returnModal.close();
    }

}
