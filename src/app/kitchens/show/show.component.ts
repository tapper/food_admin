import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
import * as moment from 'moment';
import {ActivatedRoute} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-show',
    templateUrl: './show.component.html',
    styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {

    rows: Array<any>;
    dishes: Array<any>;
    cookOrders: Array<any>;
    start = moment();
    end = moment();
    kitchen_id: number;
    gap: string = 'days';
    types = {main_dish: 0, salat: 0, sandwich: 0, fastfood: 0};
    admin: boolean = false;
    sent: boolean = false;
    deleteModal: any;
    companyToDelete: any;


    constructor(public api: ApiService, public activatedRoute: ActivatedRoute, public modalService: NgbModal) { }

    ngOnInit() {
        this.admin = localStorage.getItem('type') == '0';
        this.sent = false;
        this.activatedRoute.params.subscribe(async (data) => {
            this.kitchen_id = data.id;
            this.getOrders(data.id)
        });
    }

    getOrders (id) {
        this.sent = false;
        this.types = {main_dish: 0, salat: 0, sandwich: 0, fastfood: 0};

        this.api.sendPost('webGetOrders', {
            start: this.start.format('YYYY-MM-DD'),
            end: this.end.format('YYYY-MM-DD'),
            entity_id: id,
            entity: 'restaurant'
        }).subscribe(data => {
            this.rows = data.orders;
            this.dishes = this.rows;
            this.cookOrders = data.cook_orders;

            if (this.rows.length){
                for (let row of this.rows){
                    switch (row.food_type){
                        case '0':
                            this.types.main_dish += 1;
                            break;
                        case '1':
                            this.types.salat += 1;
                            break;
                        case '2':
                            this.types.sandwich += 1;
                            break;
                        case '3':
                            this.types.fastfood += 1;
                            break;
                        default:
                            break;
                    }
                }
            } else {
                this.types = {main_dish: 0, salat: 0, sandwich: 0, fastfood: 0};
            }
        });

    }

    selectGap(gap: string){
        this.sent = false;
        this.gap = gap;
        switch (gap){
            case 'days':
                this.start = moment();
                this.end = moment();
                this.getOrders(this.kitchen_id);
                break;
            case 'weeks':
                this.start = moment().startOf('week');
                this.end = moment().endOf('week');
                this.getOrders(this.kitchen_id);
                break;
            case 'months':
                this.start = moment().startOf('month');
                this.end = moment().endOf('month');
                this.getOrders(this.kitchen_id);
                break;
            default:
                this.start = moment();
                this.end = moment();
                this.getOrders(this.kitchen_id);
                break;
        }

    }

    assignDates(settings: string){
        this.sent = false;
        if (settings === 'forward'){
            switch (this.gap){
                case 'days':
                    this.start = moment(Object.assign({}, this.start)).add(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).add(1, 'days');
                    this.getOrders(this.kitchen_id);
                    break;
                case 'weeks':
                    this.start = moment(Object.assign({}, this.start)).add(1, 'weeks');
                    this.end = moment(Object.assign({}, this.end)).add(1, 'weeks');
                    this.getOrders(this.kitchen_id);
                    break;
                case 'months':
                    this.start = moment(Object.assign({}, this.start)).add(1, 'months');
                    this.end = moment(Object.assign({}, this.start)).endOf('month');
                    this.getOrders(this.kitchen_id);
                    break;
                default:
                    this.start = moment(Object.assign({}, this.start)).add(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).add(1, 'days');
                    this.getOrders(this.kitchen_id);
                    break;
            }
        }
        if (settings === 'backward'){
            switch (this.gap){
                case 'days':
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).subtract(1, 'days');
                    this.getOrders(this.kitchen_id);
                    break;
                case 'weeks':
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'weeks');
                    this.end = moment(Object.assign({}, this.end)).subtract(1, 'weeks');
                    this.getOrders(this.kitchen_id);
                    break;
                case 'months':
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'months');
                    this.end = moment(Object.assign({}, this.start)).endOf('month');
                    this.getOrders(this.kitchen_id);
                    break;
                default:
                    this.start = moment(Object.assign({}, this.start)).subtract(1, 'days');
                    this.end = moment(Object.assign({}, this.end)).subtract(1, 'days');
                    this.getOrders(this.kitchen_id);
                    break;
            }
        }
    }

    sendReport () {
        this.sent = false;
        this.api.sendPost('webExcelCompany', {
            start: this.start.format('YYYY-MM-DD'),
            end: this.end.format('YYYY-MM-DD'),
            entity_id: this.kitchen_id,
            entity: 'restaurant',
            type: 'admin'
        }).subscribe(() => {this.sent = true;})
    }

    sendReportToCompany () {
        this.sent = false;
        this.api.sendPost('webExcelCompany', {
            start: this.start.format('YYYY-MM-DD'),
            end: this.end.format('YYYY-MM-DD'),
            entity_id: this.kitchen_id,
            entity: 'restaurant',
            type: 'user'
        }).subscribe(() => {this.sent = true;})
    }

    sendMenu () {
        this.sent = false;
        this.api.sendPost('webExcelMenu', {
            kitchen_id: this.kitchen_id,
        }).subscribe(() => {this.sent = true;})
    }

    updateFilter (event) {
        this.sent = false;
        const val = event.target.value;
        const temp = this.dishes.filter(function(d) {
            return  d.main && d.main.name && d.main.name.toLowerCase().indexOf(val) !== -1 ||
                    d.mod1 && d.mod1.name && d.mod1.name.toLowerCase().indexOf(val) !== -1 ||
                    d.fastfood && d.fastfood.name && d.fastfood.name.toLowerCase().indexOf(val) !== -1 ||
                    d.mod2_first && d.mod2_first.name && d.mod2_first.name.toLowerCase().indexOf(val) !== -1 ||
                    d.mod2_second && d.mod2_second.name && d.mod2_second.name.toLowerCase().indexOf(val) !== -1 ||
                    d.drink && d.drink.name && d.drink.name.toLowerCase().indexOf(val) !== -1
        });
        this.rows = temp;
    }

    sendMonthReport () {
        this.sent = false;
        this.api.sendPost('webMonthReportKitchen', {
            start: this.start.format('YYYY-MM-DD'),
            end: this.end.format('YYYY-MM-DD'),
            kitchen_id: this.kitchen_id,
        }).subscribe(() => {this.sent = true;})
    }

    async deleteCompany(){
        this.api.sendPost('deleteOrder', {order_id: this.companyToDelete.index}).subscribe(data => {
           this.getOrders(this.kitchen_id);
        });
        this.deleteModal.close();

    }


    openDeleteModal(content, company) {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = company;
    }



}
