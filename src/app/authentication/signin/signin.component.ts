import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../_services/api/api.service';

@Component({
    selector: 'app-signin',
    templateUrl: './signin.component.html',
    styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

    public form: FormGroup;
    public wrongCredentials: boolean = false;

    constructor(private fb: FormBuilder, private router: Router, public api: ApiService) {}

    ngOnInit() {
        this.form = this.fb.group({
            uname: [null, Validators.compose([Validators.required])],
            password: [null, Validators.compose([Validators.required])]
        });
    }

    onSubmit() {

        this.wrongCredentials = false;

        this.api.sendPost('webLogin', {login: this.form.value.uname, password: this.form.value.password})
            .subscribe(data => {

                if (data.status == 0){
                    this.wrongCredentials = true;
                } else {

                    localStorage.setItem('id', data.company.index);
                    localStorage.setItem('type', data.company.type);
                    localStorage.setItem('image', data.company.image);
                    if (data.company.type == '0'){
                        this.router.navigate(['/']);
                    } else if (data.company.type == '1'){
                        this.router.navigate(['/companies', localStorage.getItem('id')]);
                    } else if (data.company.type == '2'){
                        this.router.navigate(['/kitchens', localStorage.getItem('id')]);
                    }

                }
            })
    }

}
