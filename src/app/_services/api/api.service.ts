import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class ApiService {

    public readonly apiRoute: string = environment.apiEndpoint;

    constructor(public http: Http) {}

    public sendGet (url: string): Observable<any> {

        return this.http.get(environment.apiEndpoint + url).map(r => {
            console.log(r.json());
            return r.json();
        });

    }

    public sendPost (url: string, data: any): Observable<any> {

        let body = new FormData();
        for (let key of Object.keys(data)){
            body.append(key, data[key]);
        }

        return this.http.post(environment.apiEndpoint + url, body).map(r => {
            console.log(r.json());
            return r.json();
        });

    }
}
