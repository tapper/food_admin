import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';

@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css'],
})
export class CreateComponent {

    public loading = false;
    company_id;
    form: FormGroup = this.fb.group({
        page: new FormControl('restaurants', Validators.required),
    });
    logo = {path: null, file: null};
    errors: Array<any> = [];

    constructor(public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public activatedRoute: ActivatedRoute,
                public api: ApiService) {}

    onFileChange(event, type) {
        let file = event.target.files[0];
        let reader = new FileReader();

        reader.onload = ev => {
            if (type === 'logo'){
                this.logo.path = this.sanitizer.bypassSecurityTrustResourceUrl((<any>ev.target).result);
                this.logo.file =  file;
            }
        };
        reader.readAsDataURL(file);
    }

    async onSubmit() {

        this.errors = [];

        if (this.form.invalid){
            this.errors.push({message: "Some data is missing"});
            return;
        }

        if (!this.logo.path){
            this.errors.push({message: "Image is missing"});
            return;
        }

        this.loading = true;

        let payload: any = {};
        payload.page = this.form.value.page;
        payload.image = this.logo.file;
        console.log(payload);

        this.api.sendPost('webAddBanner', payload).subscribe(data => {

            this.loading = false;
            this.router.navigate(['/banners']);

        }, error => {

            this.loading = false;
            this.errors.push({message: "Something is wrong, the banner wasn't created"});

        });

    }


}