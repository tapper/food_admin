import {Routes} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {CreateComponent} from './create/create.component';
import {EditComponent} from './edit/edit.component';
import {ShowComponent} from './show/show.component';

export const CompaniesRoutes: Routes = [{
    path: '',
    children: [
        {
            path: '',
            component: IndexComponent,
            data: {heading: 'הזמנות חברה'},
        },
        {
            path: 'create',
            component: CreateComponent,
            data: {heading: 'הזמנה חדשה'},
        },
        {
            path: 'edit',
            component: EditComponent,
            data: {heading: 'עריכת הזמנה'},
        },
        {
            path: ':id',
            component: ShowComponent,
            data: {heading: ''},
        }
    ]
}];
