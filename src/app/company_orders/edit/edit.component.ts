import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';
import {environment} from '../../../environments/environment';
import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-edit',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.css'],
})
export class EditComponent implements OnInit {

    public loading = false;
    admin: boolean = false;
    public order_id: number;
    public company_id: number;

    resturantsArray: Array<any>;
    menuTypesArray1: Array<any> = ["מנה מוגשת","כריכים","סלטים","מנה ארוזה"];
    menuTypesArray2: Array<any> = ["תוספות","רטבים","לחם","שתיה"];
    resturantFirstDishMenuArray : Array<any>;
    resturantTosafotArray : Array<any>;
    resturantRetavimArray : Array<any>;
    resturantBreadArray : Array<any>;
    resturantDrinksArray : Array<any>;
    tosefetQuanArray = [];
    fastfood:any;
    errors: Array<any> = [];
    model: NgbDateStruct;
    date: {year: number, month: number};
    now = new Date();


    public fields:any = {
        "fullname" : "",
        "remarks": "",
        "resturant_id" : "0",
        "dishtype" : "",
        "firstdish" : "",
        "tosefet_id" : "",
        "retavim_id" : "",
        "bread_id" : "",
        "drink_id" : "",
        "tosefet_quan" : "1",
        "deliverydate" : this.model
    }

    constructor(public fb: FormBuilder,
                private route: ActivatedRoute,
                public router:Router,
                private sanitizer: DomSanitizer,
                public api: ApiService) {
                this.route.queryParams.subscribe(params =>
                {
                    this.order_id = params['order_id'];
                    this.company_id = params['company_id'];
                    this.webgetCompanyOrdersById();
                    this.getResturants(this.company_id);
                });
                this.admin = localStorage.getItem('type') == '0';
    }

    onChangeResturant(event) {
        this.resetFields();
    }

    async onChangeFirstDishType(event) {
        await this.webgetResturantMenu();
    }

    resetFields() {
        this.fields =  {
            "fullname" : "",
            "remarks": "",
            "resturant_id" : "0",
            "dishtype" : "",
            "firstdish" : "",
            "tosefet_id" : "",
            "retavim_id" : "",
            "bread_id" : "",
            "drink_id" : "",
            "tosefet_quan" : "1",
            "deliverydate" : this.model
        }
    }

    async webgetCompanyOrdersById() {
        await this.api.sendPost('webgetCompanyOrdersById', {id:  this.order_id}).subscribe(data => {
            this.fields.fullname = data[0].fullname;
            this.fields.remarks = data[0].comments;
            this.fields.resturant_id = data[0].restaurant_id;
            this.fields.dishtype = data[0].food_type;
            this.webgetResturantMenu();
            this.fields.firstdish = data[0].main;
            this.fields.tosefet_id = data[0].mod2_first;
            this.fields.retavim_id = data[0].mod2_second;
            this.fields.bread_id = data[0].mod1;
            this.fields.drink_id = data[0].drink;
            this.fields.tosefet_quan = data[0].quantity;
            let splitdate = data[0].order_date.split("-");
            this.fields.deliverydate = {year: parseInt(splitdate[0]), month: parseInt(splitdate[1]), day: parseInt(splitdate[2])};
             console.log ("webgetCompanyOrdersById",data);
        });
    }

    selectToday() {
        this.model = {year: 2017, month: this.now.getMonth() + 1, day: this.now.getDate()};
    }

    async webgetResturantMenu() {
        if  (this.fields.resturant_id && this.fields.dishtype) {
            await this.api.sendPost('webgetResturantMenu', {resturant_id:  this.fields.resturant_id,food_type: this.fields.dishtype}).subscribe(data => {
                this.resturantFirstDishMenuArray = data.firstdish;
                this.resturantTosafotArray = data.tosefet;
                this.resturantRetavimArray = data.retavim;
                this.resturantBreadArray = data.bread;
                this.resturantDrinksArray = data.drinks;
            });
        }
    }

    async ngOnInit() {

        for (let i = 1; i <= 20; i++) {
            this.tosefetQuanArray.push(i);
        }

        this.selectToday();

    }


    getResturants (id) {
        this.api.sendPost('getUserResturants', {companyid: id}).subscribe(data => {
            this.resturantsArray = data;
            console.log("resturantsArray",this.resturantsArray)
        });
    }


    onSubmit(form: NgForm) {

        console.log("form",form.value);

        if (this.fields.dishtype == 3)
            this.fastfood = this.fields.firstdish;
        else
            this.fastfood = 0;

        this.api.sendPost('webUpdateCompanyOrder', {
            'order_id' : this.order_id,
            //'company_id':  this.company_id,
            'restaurant_id':  this.fields.resturant_id,
            'fullname':  this.fields.fullname,
            'food_type':  this.fields.dishtype,
            'main':  this.fields.firstdish,
            'mod1':  this.fields.bread_id,
            'mod2_first':  this.fields.tosefet_id,
            'mod2_second':  this.fields.retavim_id,
            'drink':  this.fields.drink_id,
            'fastfood':  this.fastfood,
            'comments':  this.fields.remarks,
            'quantity':  this.fields.tosefet_quan,
            'order_date_year':  this.fields.deliverydate.year,//moment(this.start).format("YYYY-MM-DD"),
            'order_date_month':  this.fields.deliverydate.month,//moment(this.start).format("YYYY-MM-DD"),
            'order_date_day':  this.fields.deliverydate.day,//moment(this.start).format("YYYY-MM-DD"),
            //'order_date':  this.fields.deliverydate//moment(this.start).format("YYYY-MM-DD"),
            }
        ).subscribe(data =>
        {
            console.log("savedData",data);
            if  (this.admin)
                     this.router.navigateByUrl('/company_orders');
                else
                    this.router.navigateByUrl('/company_orders/'+this.company_id);

        });

    }



}