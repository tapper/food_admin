import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
//import * as moment from 'moment';
import { MomentModule } from 'angular2-moment';
import {ActivatedRoute,Router} from '@angular/router';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-show',
    templateUrl: './show.component.html',
    styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {


    admin: boolean = false;
    public company_id: number;
    types = {main_dish: 0, salat: 0, sandwich: 0, fastfood: 0};
    rows: Array<any>;
    deleteModal: any;
    companyToDelete: any;

    constructor(public api: ApiService, public activatedRoute: ActivatedRoute, private router: Router, public modalService: NgbModal) { }

    ngOnInit() {
        this.admin = localStorage.getItem('type') == '0';
        this.activatedRoute.params.subscribe(async (data) => {
            this.company_id = data.id;
            this.getCompanyOrders(this.company_id);
        });

    }

    getCompanyOrders(company_id) {
        this.api.sendPost('webGetCompanyOrders', {'company_id':  this.company_id,}).subscribe(data =>
        {
            this.rows = data.orders;
            if (this.rows.length){
                for (let row of this.rows){
                    switch (row.food_type){
                        case '0':
                            this.types.main_dish += 1;
                            break;
                        case '1':
                            this.types.salat += 1;
                            break;
                        case '2':
                            this.types.sandwich += 1;
                            break;
                        case '3':
                            this.types.fastfood += 1;
                            break;
                        default:
                            break;
                    }
                }
            } else {
                this.types = {main_dish: 0, salat: 0, sandwich: 0, fastfood: 0};
            }
            console.log("webGetCompanyOrders",data.orders);
        });
    }

    neworderpage() {
        this.router.navigate(['/','company_orders','create'], { queryParams: { company_id: this.company_id } });
    }

    goEditPage(order_id) {
        this.router.navigate(['/','company_orders','edit'], { queryParams: { company_id: this.company_id,order_id: order_id } });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.rows.filter(function(d) {
            return d.fullname && d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    async deleteCompany(){
        this.api.sendPost('webDeleteCompanyOrder', {id: this.companyToDelete.id}).subscribe(data => {
            this.getCompanyOrders(this.company_id);
        });
        this.deleteModal.close();
    }

    openDeleteModal(content, company) {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = company;
    }
}
