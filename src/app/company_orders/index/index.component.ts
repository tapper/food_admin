import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../_services/api/api.service';
//import * as moment from 'moment';
import { MomentModule } from 'angular2-moment';
import {ActivatedRoute,Router} from '@angular/router';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

    admin: boolean = false;
    rows: Array<any>;
    companies: Array<any>;
    deleteModal: any;
    pushModal: any;
    companyToDelete: any;
    companyToPush: any;
    pushText: string = '';
    sent: boolean = false;
    inProcess: boolean = false;
    public company_id:number;

    constructor(public api: ApiService, public activatedRoute: ActivatedRoute, private router: Router, public modalService: NgbModal) { }

    ngOnInit() {
        this.admin = localStorage.getItem('type') == '0';
        this.company_id= -1;
        this.getOrders();
    }

    getOrders () {
        this.api.sendPost('webGetCompanyOrders', {'company_id':  -1}).subscribe(data => {
            this.rows = data.orders;
            this.companies = this.rows;
        });
    }

    updateFilter(event) {
        const val = event.target.value;
        const temp = this.companies.filter(function(d) {
            return d.name && d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });
        this.rows = temp;
    }

    neworderpage() {
        this.router.navigate(['/','company_orders','create'], { queryParams: { company_id: this.company_id } });
    }

    goEditPage(order_id,company_id) {
        this.router.navigate(['/','company_orders','edit'], { queryParams: { company_id: company_id,order_id: order_id } });
    }


    async deleteCompany(){
        this.api.sendPost('webDeleteCompanyOrder', {id: this.companyToDelete.id}).subscribe(data => {
            this.getOrders();
        });
        this.deleteModal.close();
    }

    openDeleteModal(content, company) {
        this.deleteModal = this.modalService.open(content);
        this.companyToDelete = company;
    }


}
