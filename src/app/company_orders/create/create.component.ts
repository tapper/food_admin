import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators, NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {DomSanitizer} from '@angular/platform-browser';
import {ApiService} from '../../_services/api/api.service';
import * as Quill from 'quill';
import {NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";
//import moment = require("moment");

@Component({
    selector: 'app-create',
    changeDetection: ChangeDetectionStrategy.Default,
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {

    public loading = false;
    resturantsArray: Array<any>;
    menuTypesArray1: Array<any> = ["מנה מוגשת","כריכים","סלטים","מנה ארוזה"];
    menuTypesArray2: Array<any> = ["תוספות","רטבים","לחם","שתיה"];
    resturantFirstDishMenuArray : Array<any>;
    resturantTosafotArray : Array<any>;
    resturantRetavimArray : Array<any>;
    resturantBreadArray : Array<any>;
    resturantDrinksArray : Array<any>;
    tosefetQuanArray = [];
    fastfood:any;
    errors: Array<any> = [];
    public company_id:any;
    model: NgbDateStruct;
    date: {year: number, month: number};
    now = new Date();



    public fields:any = {
        "fullname" : "",
        "remarks": "",
        "resturant_id" : "0",
        "dishtype" : "",
        "firstdish" : "",
        "tosefet_id" : "",
        "retavim_id" : "",
        "bread_id" : "",
        "drink_id" : "",
        "tosefet_quan" : "1",
        "deliverydate" : ""
    }




    constructor(public fb: FormBuilder,
                private router: Router,
                private sanitizer: DomSanitizer,
                public api: ApiService , private route: ActivatedRoute) {

        this.route.queryParams.subscribe(params => {
            this.company_id =   params['company_id'];
            this.getResturants(this.company_id);
        });
    }

    getResturants (id) {
        this.api.sendPost('getUserResturants', {companyid: id}).subscribe(data => {
            this.resturantsArray = data;
            console.log("resturantsArray",this.resturantsArray)
        });
    }

    onChangeResturant(event) {
    }

    onChangeFirstDishType(event) {
        this.webgetResturantMenu();
    }

    webgetResturantMenu() {
        if  (this.fields.resturant_id && this.fields.dishtype) {
            this.api.sendPost('webgetResturantMenu', {resturant_id:  this.fields.resturant_id,food_type: this.fields.dishtype}).subscribe(data => {
                this.resturantFirstDishMenuArray = data.firstdish;
                this.resturantTosafotArray = data.tosefet;
                this.resturantRetavimArray = data.retavim;
                this.resturantBreadArray = data.bread;
                this.resturantDrinksArray = data.drinks;
            });
        }
    }

    async ngOnInit() {


        for (let i = 1; i <= 20; i++) {
            this.tosefetQuanArray.push(i);
        }
        this.fields.deliverydate = {year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate()};

    }



    onSubmit(form: NgForm) {
        console.log("form",form.value);

        if (this.fields.dishtype == 3)
            this.fastfood = this.fields.firstdish;
        else
            this.fastfood = 0;



        this.api.sendPost('webSaveCompanyOrder', {
                'company_id':  this.company_id,
                'restaurant_id':  this.fields.resturant_id,
                'fullname':  this.fields.fullname,
                'food_type':  this.fields.dishtype,
                'main':  this.fields.firstdish,
                'mod1':  this.fields.bread_id,
                'mod2_first':  this.fields.tosefet_id,
                'mod2_second':  this.fields.retavim_id,
                'drink':  this.fields.drink_id,
                'fastfood':  this.fastfood,
                'comments':  this.fields.remarks,
                'quantity':  this.fields.tosefet_quan,
                'order_date_year':  this.fields.deliverydate.year,//moment(this.start).format("YYYY-MM-DD"),
                'order_date_month':  this.fields.deliverydate.month,//moment(this.start).format("YYYY-MM-DD"),
                'order_date_day':  this.fields.deliverydate.day,//moment(this.start).format("YYYY-MM-DD"),
            //'order_date':  this.fields.deliverydate//moment(this.start).format("YYYY-MM-DD"),
            }
        ).subscribe(data =>
        {
            console.log("savedData",data);
            this.router.navigateByUrl('/company_orders/'+this.company_id);
        });


    }



}