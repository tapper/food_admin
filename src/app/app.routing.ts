import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';
import {AuthGuard} from './_guards/auth.guard';

export const AppRoutes: Routes = [{
    path: '',
    component: AdminLayoutComponent,
    children: [
        {
            path: '',
            loadChildren: './dashboard/dashboard.module#DashboardModule'
        },
        {
            path: 'profile',
            loadChildren: './profile/profile.module#ProfileModule'
        },
        {
            path: 'birthdays',
            loadChildren: './birthdays/birthdays.module#BirthdaysModule'
        },
        {
            path: 'banners',
            loadChildren: './banners/banners.module#BannersModule'
        },
        {
            path: 'companies',
            loadChildren: './companies/companies.module#CompaniesModule'
        },
        {
            path: 'company_orders',
            loadChildren: './company_orders/companies.module#CompaniesModule'
        },
        {
            path: 'company_firstdish_order',
            loadChildren: './company_firstdish_order/companies.module#CompaniesModule'
        },
        {
            path: 'custom-meal',
            loadChildren: './custom-meal/custom-meal.module#CompaniesModule'
        },
        {
            path: 'companies/:id/employees',
            loadChildren: './employees/employees.module#EmployeesModule'
        },
        {
            path: 'kitchens',
            loadChildren: './kitchens/kitchens.module#KitchensModule'
        },
        {
            path: 'kitchens/:id/categories',
            loadChildren: './categories/categories.module#CategoriesModule'
        },
        {
            path: 'kitchens/:id/categories/:category_id/dishes',
            loadChildren: './dishes/dishes.module#DishesModule'
        },
        {
            path: 'email',
            loadChildren: './email/email.module#EmailModule'
        }, {
            path: 'components',
            loadChildren: './components/components.module#ComponentsModule'
        }, {
            path: 'icons',
            loadChildren: './icons/icons.module#IconsModule'
        }, {
            path: 'cards',
            loadChildren: './cards/cards.module#CardsModule'
        }, {
            path: 'forms',
            loadChildren: './form/form.module#FormModule'
        }, {
            path: 'tables',
            loadChildren: './tables/tables.module#TablesModule'
        }, {
            path: 'datatable',
            loadChildren: './datatable/datatable.module#DatatableModule'
        }, {
            path: 'charts',
            loadChildren: './charts/charts.module#ChartsModule'
        }, {
            path: 'maps',
            loadChildren: './maps/maps.module#MapsModule'
        }, {
            path: 'pages',
            loadChildren: './pages/pages.module#PagesModule'
        }, {
            path: 'taskboard',
            loadChildren: './taskboard/taskboard.module#TaskboardModule'
        }, {
            path: 'calendar',
            loadChildren: './fullcalendar/fullcalendar.module#FullcalendarModule'
        }, {
            path: 'media',
            loadChildren: './media/media.module#MediaModule'
        }, {
            path: 'widgets',
            loadChildren: './widgets/widgets.module#WidgetsModule'
        }, {
            path: 'social',
            loadChildren: './social/social.module#SocialModule'
        }, {
            path: 'docs',
            loadChildren: './docs/docs.module#DocsModule'
        }],
    canActivate: [AuthGuard]
}, {
    path: '',
    component: AuthLayoutComponent,
    children: [
        {
            path: 'authentication',
            loadChildren: './authentication/authentication.module#AuthenticationModule'
        }, {
            path: 'error',
            loadChildren: './error/error.module#ErrorModule'
        }, {
            path: 'landing',
            loadChildren: './landing/landing.module#LandingModule'
        }]
}, {
    path: '**',
    redirectTo: 'error/404'
}];

